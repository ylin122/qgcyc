/****************************************************************************
** Meta object code from reading C++ file 'dialog_bare.h'
**
** Created: Sat Mar 15 15:44:52 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qupgrade/src/apps/qupgrade/dialog_bare.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dialog_bare.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DialogBare[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   12,   11,   11, 0x05,
      42,   11,   11,   11, 0x05,
      57,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      75,   11,   11,   11, 0x0a,
      98,   11,   11,   11, 0x0a,
     120,  112,   11,   11, 0x0a,
     141,   11,   11,   11, 0x0a,
     193,  155,   11,   11, 0x0a,
     240,  236,   11,   11, 0x0a,
     271,  260,   11,   11, 0x0a,
     310,  305,   11,   11, 0x08,
     337,   11,   11,   11, 0x08,
     361,   11,   11,   11, 0x08,
     385,   11,   11,   11, 0x08,
     409,   11,   11,   11, 0x08,
     420,   11,   11,   11, 0x08,
     449,  441,   11,   11, 0x08,
     486,   11,   11,   11, 0x08,
     514,  506,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_DialogBare[] = {
    "DialogBare\0\0filename\0filenameSet(QString)\0"
    "connectLinks()\0disconnectLinks()\0"
    "onPortAddedOrRemoved()\0onLoadStart()\0"
    "success\0onLoadFinished(bool)\0onUserAbort()\0"
    "success,board_id,boardName,bootLoader\0"
    "onDetectFinished(bool,int,QString,QString)\0"
    "url\0onFlashURL(QString)\0curr,total\0"
    "onDownloadProgress(qint64,qint64)\0"
    "name\0onPortNameChanged(QString)\0"
    "onFileSelectRequested()\0onCancelButtonClicked()\0"
    "onUploadButtonClicked()\0onDetect()\0"
    "onDownloadFinished()\0request\0"
    "onDownloadRequested(QNetworkRequest)\0"
    "onLinkClicked(QUrl)\0enabled\0"
    "onToggleAdvancedMode(bool)\0"
};

void DialogBare::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DialogBare *_t = static_cast<DialogBare *>(_o);
        switch (_id) {
        case 0: _t->filenameSet((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->connectLinks(); break;
        case 2: _t->disconnectLinks(); break;
        case 3: _t->onPortAddedOrRemoved(); break;
        case 4: _t->onLoadStart(); break;
        case 5: _t->onLoadFinished((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->onUserAbort(); break;
        case 7: _t->onDetectFinished((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4]))); break;
        case 8: _t->onFlashURL((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->onDownloadProgress((*reinterpret_cast< qint64(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2]))); break;
        case 10: _t->onPortNameChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: _t->onFileSelectRequested(); break;
        case 12: _t->onCancelButtonClicked(); break;
        case 13: _t->onUploadButtonClicked(); break;
        case 14: _t->onDetect(); break;
        case 15: _t->onDownloadFinished(); break;
        case 16: _t->onDownloadRequested((*reinterpret_cast< const QNetworkRequest(*)>(_a[1]))); break;
        case 17: _t->onLinkClicked((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 18: _t->onToggleAdvancedMode((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DialogBare::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DialogBare::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_DialogBare,
      qt_meta_data_DialogBare, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DialogBare::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DialogBare::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DialogBare::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DialogBare))
        return static_cast<void*>(const_cast< DialogBare*>(this));
    return QWidget::qt_metacast(_clname);
}

int DialogBare::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
    return _id;
}

// SIGNAL 0
void DialogBare::filenameSet(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void DialogBare::connectLinks()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void DialogBare::disconnectLinks()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE
