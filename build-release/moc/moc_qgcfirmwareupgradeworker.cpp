/****************************************************************************
** Meta object code from reading C++ file 'qgcfirmwareupgradeworker.h'
**
** Created: Sat Mar 15 15:44:52 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qupgrade/src/apps/qupgrade/qgcfirmwareupgradeworker.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgcfirmwareupgradeworker.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_QGCFirmwareUpgradeWorker[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: signature, parameters, type, tag, flags
      33,   26,   25,   25, 0x05,
      65,   26,   25,   25, 0x05,
     103,   95,   25,   25, 0x05,
     140,  131,   25,   25, 0x05,
     172,  164,   25,   25, 0x05,
     229,  191,   25,   25, 0x05,
     270,   25,   25,   25, 0x05,

 // slots: signature, parameters, type, tag, flags
     281,   25,   25,   25, 0x0a,
     302,   25,   25,   25, 0x0a,
     325,  316,   25,   25, 0x0a,
     346,   25,   25,   25, 0x0a,
     361,   25,   25,   25, 0x0a,
     379,  376,   25,   25, 0x0a,
     401,  395,   25,   25, 0x0a,
     428,  423,   25,   25, 0x0a,
     445,   25,   25,   25, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_QGCFirmwareUpgradeWorker[] = {
    "QGCFirmwareUpgradeWorker\0\0status\0"
    "detectionStatusChanged(QString)\0"
    "upgradeStatusChanged(QString)\0percent\0"
    "upgradeProgressChanged(int)\0portName\0"
    "validPortFound(QString)\0success\0"
    "loadFinished(bool)\0"
    "success,board_id,boardName,bootLoader\0"
    "detectFinished(bool,int,QString,QString)\0"
    "finished()\0startContinousScan()\0"
    "abortUpload()\0filename\0setFilename(QString)\0"
    "loadFirmware()\0detectBoards()\0id\0"
    "setBoardId(int)\0abort\0setAbortOnError(bool)\0"
    "port\0setPort(QString)\0abort()\0"
};

void QGCFirmwareUpgradeWorker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        QGCFirmwareUpgradeWorker *_t = static_cast<QGCFirmwareUpgradeWorker *>(_o);
        switch (_id) {
        case 0: _t->detectionStatusChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->upgradeStatusChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->upgradeProgressChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->validPortFound((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->loadFinished((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->detectFinished((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4]))); break;
        case 6: _t->finished(); break;
        case 7: _t->startContinousScan(); break;
        case 8: _t->abortUpload(); break;
        case 9: _t->setFilename((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: _t->loadFirmware(); break;
        case 11: _t->detectBoards(); break;
        case 12: _t->setBoardId((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->setAbortOnError((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->setPort((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: _t->abort(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData QGCFirmwareUpgradeWorker::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QGCFirmwareUpgradeWorker::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QGCFirmwareUpgradeWorker,
      qt_meta_data_QGCFirmwareUpgradeWorker, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QGCFirmwareUpgradeWorker::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QGCFirmwareUpgradeWorker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QGCFirmwareUpgradeWorker::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QGCFirmwareUpgradeWorker))
        return static_cast<void*>(const_cast< QGCFirmwareUpgradeWorker*>(this));
    return QObject::qt_metacast(_clname);
}

int QGCFirmwareUpgradeWorker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void QGCFirmwareUpgradeWorker::detectionStatusChanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QGCFirmwareUpgradeWorker::upgradeStatusChanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QGCFirmwareUpgradeWorker::upgradeProgressChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QGCFirmwareUpgradeWorker::validPortFound(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QGCFirmwareUpgradeWorker::loadFinished(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QGCFirmwareUpgradeWorker::detectFinished(bool _t1, int _t2, const QString & _t3, const QString & _t4)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QGCFirmwareUpgradeWorker::finished()
{
    QMetaObject::activate(this, &staticMetaObject, 6, 0);
}
QT_END_MOC_NAMESPACE
