/********************************************************************************
** Form generated from reading UI file 'QGCMessageView.ui'
**
** Created: Sat Mar 15 15:42:58 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QGCMESSAGEVIEW_H
#define UI_QGCMESSAGEVIEW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QScrollArea>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QGCMessageView
{
public:
    QHBoxLayout *horizontalLayout_2;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QHBoxLayout *horizontalLayout;
    QPlainTextEdit *plainTextEdit;

    void setupUi(QWidget *QGCMessageView)
    {
        if (QGCMessageView->objectName().isEmpty())
            QGCMessageView->setObjectName(QString::fromUtf8("QGCMessageView"));
        QGCMessageView->resize(305, 283);
        horizontalLayout_2 = new QHBoxLayout(QGCMessageView);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 8, 0, 0);
        scrollArea = new QScrollArea(QGCMessageView);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 303, 273));
        horizontalLayout = new QHBoxLayout(scrollAreaWidgetContents);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        plainTextEdit = new QPlainTextEdit(scrollAreaWidgetContents);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));
        plainTextEdit->setReadOnly(true);

        horizontalLayout->addWidget(plainTextEdit);

        scrollArea->setWidget(scrollAreaWidgetContents);

        horizontalLayout_2->addWidget(scrollArea);


        retranslateUi(QGCMessageView);

        QMetaObject::connectSlotsByName(QGCMessageView);
    } // setupUi

    void retranslateUi(QWidget *QGCMessageView)
    {
        QGCMessageView->setWindowTitle(QApplication::translate("QGCMessageView", "Form", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class QGCMessageView: public Ui_QGCMessageView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QGCMESSAGEVIEW_H
