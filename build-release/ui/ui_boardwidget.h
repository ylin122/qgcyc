/********************************************************************************
** Form generated from reading UI file 'boardwidget.ui'
**
** Created: Sat Mar 15 15:42:58 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BOARDWIDGET_H
#define UI_BOARDWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_boardWidget
{
public:
    QGridLayout *gridLayout_2;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *horizontalSpacer_3;
    QWidget *containerWidget;
    QGridLayout *gridLayout;
    QPushButton *cancelButton;
    QLabel *bootloaderLabel;
    QLabel *boardNameLabel;
    QComboBox *firmwareComboBox;
    QLabel *iconLabel;
    QLabel *statusLabel;
    QSpacerItem *verticalSpacer;
    QPushButton *flashButton;

    void setupUi(QWidget *boardWidget)
    {
        if (boardWidget->objectName().isEmpty())
            boardWidget->setObjectName(QString::fromUtf8("boardWidget"));
        boardWidget->resize(670, 302);
        boardWidget->setStyleSheet(QString::fromUtf8("\n"
"\n"
"QLabel#boardNameLabel {\n"
"	font-size: 20pt;\n"
"	font-weight: bold;\n"
"}\n"
"\n"
"QLabel#statusLabel {\n"
"    background-color: none;\n"
"    border: none;\n"
"	font-size: 12pt;\n"
"	color: rgb(121, 255, 149);\n"
"}\n"
"\n"
"QPushButton#flashButton {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #73D95D, stop: 1 #18A154);\n"
"}"));
        gridLayout_2 = new QGridLayout(boardWidget);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        verticalSpacer_2 = new QSpacerItem(478, 82, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer_2, 1, 0, 1, 3);

        horizontalSpacer_2 = new QSpacerItem(22, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 0, 0, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(22, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_3, 0, 2, 1, 1);

        containerWidget = new QWidget(boardWidget);
        containerWidget->setObjectName(QString::fromUtf8("containerWidget"));
        gridLayout = new QGridLayout(containerWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        cancelButton = new QPushButton(containerWidget);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));

        gridLayout->addWidget(cancelButton, 3, 4, 1, 1);

        bootloaderLabel = new QLabel(containerWidget);
        bootloaderLabel->setObjectName(QString::fromUtf8("bootloaderLabel"));
        bootloaderLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout->addWidget(bootloaderLabel, 1, 2, 1, 3);

        boardNameLabel = new QLabel(containerWidget);
        boardNameLabel->setObjectName(QString::fromUtf8("boardNameLabel"));
        boardNameLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout->addWidget(boardNameLabel, 0, 2, 1, 3);

        firmwareComboBox = new QComboBox(containerWidget);
        firmwareComboBox->setObjectName(QString::fromUtf8("firmwareComboBox"));

        gridLayout->addWidget(firmwareComboBox, 3, 2, 1, 1);

        iconLabel = new QLabel(containerWidget);
        iconLabel->setObjectName(QString::fromUtf8("iconLabel"));
        iconLabel->setMinimumSize(QSize(200, 100));

        gridLayout->addWidget(iconLabel, 0, 0, 5, 2);

        statusLabel = new QLabel(containerWidget);
        statusLabel->setObjectName(QString::fromUtf8("statusLabel"));

        gridLayout->addWidget(statusLabel, 5, 0, 1, 5);

        verticalSpacer = new QSpacerItem(148, 41, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 2, 2, 1, 3);

        flashButton = new QPushButton(containerWidget);
        flashButton->setObjectName(QString::fromUtf8("flashButton"));

        gridLayout->addWidget(flashButton, 3, 3, 1, 1);


        gridLayout_2->addWidget(containerWidget, 0, 1, 1, 1);

        gridLayout_2->setRowStretch(0, 1);
        gridLayout_2->setColumnStretch(0, 50);
        gridLayout_2->setColumnStretch(2, 50);

        retranslateUi(boardWidget);

        QMetaObject::connectSlotsByName(boardWidget);
    } // setupUi

    void retranslateUi(QWidget *boardWidget)
    {
        boardWidget->setWindowTitle(QApplication::translate("boardWidget", "Form", 0, QApplication::UnicodeUTF8));
        cancelButton->setText(QApplication::translate("boardWidget", "Cancel", 0, QApplication::UnicodeUTF8));
        bootloaderLabel->setText(QApplication::translate("boardWidget", "Bootloader v 3.0", 0, QApplication::UnicodeUTF8));
        boardNameLabel->setText(QApplication::translate("boardWidget", "PX4FMU v1.x", 0, QApplication::UnicodeUTF8));
        iconLabel->setText(QApplication::translate("boardWidget", "ICON", 0, QApplication::UnicodeUTF8));
        statusLabel->setText(QApplication::translate("boardWidget", "Fetching Firmware options..", 0, QApplication::UnicodeUTF8));
        flashButton->setText(QApplication::translate("boardWidget", "Upgrade", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class boardWidget: public Ui_boardWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BOARDWIDGET_H
