/********************************************************************************
** Form generated from reading UI file 'dialog_bare.ui'
**
** Created: Sat Mar 15 15:42:58 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_BARE_H
#define UI_DIALOG_BARE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DialogBare
{
public:
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayoutBrowser;
    QCheckBox *advancedCheckBox;
    QPushButton *cancelButton;
    QLabel *boardIdLabel;
    QPlainTextEdit *upgradeLog;
    QWidget *boardListWidget;
    QGridLayout *boardListLayout;
    QPushButton *scanButton;
    QLabel *boardListLabel;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer;
    QPushButton *selectFileButton;
    QProgressBar *upgradeProgressBar;
    QComboBox *boardComboBox;
    QComboBox *portBox;
    QPushButton *flashButton;
    QLabel *portLabel;

    void setupUi(QWidget *DialogBare)
    {
        if (DialogBare->objectName().isEmpty())
            DialogBare->setObjectName(QString::fromUtf8("DialogBare"));
        DialogBare->resize(660, 410);
        DialogBare->setMinimumSize(QSize(600, 200));
        horizontalLayout_2 = new QHBoxLayout(DialogBare);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayoutBrowser = new QGridLayout();
        gridLayoutBrowser->setSpacing(6);
        gridLayoutBrowser->setObjectName(QString::fromUtf8("gridLayoutBrowser"));
        advancedCheckBox = new QCheckBox(DialogBare);
        advancedCheckBox->setObjectName(QString::fromUtf8("advancedCheckBox"));

        gridLayoutBrowser->addWidget(advancedCheckBox, 0, 0, 1, 1);

        cancelButton = new QPushButton(DialogBare);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));
        cancelButton->setEnabled(false);

        gridLayoutBrowser->addWidget(cancelButton, 3, 8, 1, 1);

        boardIdLabel = new QLabel(DialogBare);
        boardIdLabel->setObjectName(QString::fromUtf8("boardIdLabel"));

        gridLayoutBrowser->addWidget(boardIdLabel, 0, 4, 1, 1);

        upgradeLog = new QPlainTextEdit(DialogBare);
        upgradeLog->setObjectName(QString::fromUtf8("upgradeLog"));
        upgradeLog->setReadOnly(true);
        upgradeLog->setMaximumBlockCount(800);

        gridLayoutBrowser->addWidget(upgradeLog, 2, 0, 1, 9);

        boardListWidget = new QWidget(DialogBare);
        boardListWidget->setObjectName(QString::fromUtf8("boardListWidget"));
        boardListWidget->setStyleSheet(QString::fromUtf8("QWidget#boardListWidget {\n"
"    background-color: #555555;\n"
"    border-radius: 16px;\n"
"    border: 2px dashed #404040;\n"
"}\n"
"\n"
"QLabel#boardListLabel {\n"
"    background-color: none;\n"
"    border: none;\n"
"	font-size: 20pt;\n"
"	font-weight: bold;\n"
"	color: #888888;\n"
"}\n"
"\n"
"QPushButton#scanButton {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #73D95D, stop: 1 #18A154);\n"
"    border-radius: 8px;\n"
"    min-height: 40px;\n"
"    min-width: 320px;\n"
"    max-width: 540px;\n"
"	font-size: 20pt;\n"
"	color: #366158;\n"
"    border: 3px solid #36A158;\n"
"}"));
        boardListLayout = new QGridLayout(boardListWidget);
        boardListLayout->setSpacing(6);
        boardListLayout->setContentsMargins(11, 11, 11, 11);
        boardListLayout->setObjectName(QString::fromUtf8("boardListLayout"));
        scanButton = new QPushButton(boardListWidget);
        scanButton->setObjectName(QString::fromUtf8("scanButton"));

        boardListLayout->addWidget(scanButton, 1, 1, 1, 1);

        boardListLabel = new QLabel(boardListWidget);
        boardListLabel->setObjectName(QString::fromUtf8("boardListLabel"));
        boardListLabel->setAlignment(Qt::AlignCenter);

        boardListLayout->addWidget(boardListLabel, 0, 0, 1, 3);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        boardListLayout->addItem(horizontalSpacer, 1, 0, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        boardListLayout->addItem(horizontalSpacer_2, 1, 2, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        boardListLayout->addItem(verticalSpacer, 2, 0, 1, 3);

        boardListLayout->setRowStretch(0, 1);
        boardListLayout->setRowStretch(1, 100);

        gridLayoutBrowser->addWidget(boardListWidget, 1, 0, 1, 9);

        selectFileButton = new QPushButton(DialogBare);
        selectFileButton->setObjectName(QString::fromUtf8("selectFileButton"));

        gridLayoutBrowser->addWidget(selectFileButton, 0, 6, 1, 1);

        upgradeProgressBar = new QProgressBar(DialogBare);
        upgradeProgressBar->setObjectName(QString::fromUtf8("upgradeProgressBar"));
        upgradeProgressBar->setValue(0);

        gridLayoutBrowser->addWidget(upgradeProgressBar, 3, 0, 1, 8);

        boardComboBox = new QComboBox(DialogBare);
        boardComboBox->setObjectName(QString::fromUtf8("boardComboBox"));

        gridLayoutBrowser->addWidget(boardComboBox, 0, 5, 1, 1);

        portBox = new QComboBox(DialogBare);
        portBox->setObjectName(QString::fromUtf8("portBox"));

        gridLayoutBrowser->addWidget(portBox, 0, 2, 1, 2);

        flashButton = new QPushButton(DialogBare);
        flashButton->setObjectName(QString::fromUtf8("flashButton"));

        gridLayoutBrowser->addWidget(flashButton, 0, 8, 1, 1);

        portLabel = new QLabel(DialogBare);
        portLabel->setObjectName(QString::fromUtf8("portLabel"));
        portLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayoutBrowser->addWidget(portLabel, 0, 1, 1, 1);

        gridLayoutBrowser->setRowStretch(0, 1);
        gridLayoutBrowser->setRowStretch(1, 10);
        gridLayoutBrowser->setRowStretch(2, 15);
        gridLayoutBrowser->setRowStretch(3, 1);
        gridLayoutBrowser->setColumnStretch(0, 1);
        gridLayoutBrowser->setRowMinimumHeight(0, 15);
        gridLayoutBrowser->setRowMinimumHeight(1, 100);
        gridLayoutBrowser->setRowMinimumHeight(2, 100);
        gridLayoutBrowser->setRowMinimumHeight(3, 15);

        verticalLayout->addLayout(gridLayoutBrowser);


        horizontalLayout_2->addLayout(verticalLayout);

        horizontalLayout_2->setStretch(0, 50);

        retranslateUi(DialogBare);

        QMetaObject::connectSlotsByName(DialogBare);
    } // setupUi

    void retranslateUi(QWidget *DialogBare)
    {
        DialogBare->setWindowTitle(QApplication::translate("DialogBare", "DialogBare", 0, QApplication::UnicodeUTF8));
        advancedCheckBox->setText(QApplication::translate("DialogBare", "Advanced", 0, QApplication::UnicodeUTF8));
        cancelButton->setText(QApplication::translate("DialogBare", "Cancel", 0, QApplication::UnicodeUTF8));
        boardIdLabel->setText(QApplication::translate("DialogBare", "Board", 0, QApplication::UnicodeUTF8));
        scanButton->setText(QApplication::translate("DialogBare", "Scan", 0, QApplication::UnicodeUTF8));
        boardListLabel->setText(QApplication::translate("DialogBare", "Please scan to identify PX4 boards.", 0, QApplication::UnicodeUTF8));
        selectFileButton->setText(QApplication::translate("DialogBare", "Select File", 0, QApplication::UnicodeUTF8));
        portBox->clear();
        portBox->insertItems(0, QStringList()
         << QApplication::translate("DialogBare", "<Automatic>", 0, QApplication::UnicodeUTF8)
        );
        flashButton->setText(QApplication::translate("DialogBare", "Flash", 0, QApplication::UnicodeUTF8));
        portLabel->setText(QApplication::translate("DialogBare", "Port:", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class DialogBare: public Ui_DialogBare {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_BARE_H
