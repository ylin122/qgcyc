/** @file
 *	@brief MAVLink comm protocol testsuite generated from yc_msg.xml
 *	@see http://qgroundcontrol.org/mavlink/
 */
#ifndef YC_MSG_TESTSUITE_H
#define YC_MSG_TESTSUITE_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef MAVLINK_TEST_ALL
#define MAVLINK_TEST_ALL
static void mavlink_test_common(uint8_t, uint8_t, mavlink_message_t *last_msg);
static void mavlink_test_yc_msg(uint8_t, uint8_t, mavlink_message_t *last_msg);

static void mavlink_test_all(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_test_common(system_id, component_id, last_msg);
	mavlink_test_yc_msg(system_id, component_id, last_msg);
}
#endif

#include "../common/testsuite.h"


static void mavlink_test_adsb_msg(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_adsb_msg_t packet_in = {
		17.0,
	}45.0,
	}17651,
	}17755,
	}17859,
	}175,
	}242,
	}{ 53, 54, 55 },
	}254,
	}65,
	}132,
	}199,
	};
	mavlink_adsb_msg_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.latitude = packet_in.latitude;
        	packet1.longitude = packet_in.longitude;
        	packet1.altitude = packet_in.altitude;
        	packet1.h_velocity = packet_in.h_velocity;
        	packet1.v_velocity = packet_in.v_velocity;
        	packet1.traffic_alert_status = packet_in.traffic_alert_status;
        	packet1.address_type = packet_in.address_type;
        	packet1.misc_indicator = packet_in.misc_indicator;
        	packet1.NIC = packet_in.NIC;
        	packet1.NACp = packet_in.NACp;
        	packet1.heading = packet_in.heading;
        
        	mav_array_memcpy(packet1.address, packet_in.address, sizeof(int8_t)*3);
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_adsb_msg_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_adsb_msg_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_adsb_msg_pack(system_id, component_id, &msg , packet1.traffic_alert_status , packet1.address_type , packet1.address , packet1.latitude , packet1.longitude , packet1.altitude , packet1.misc_indicator , packet1.NIC , packet1.NACp , packet1.h_velocity , packet1.v_velocity , packet1.heading );
	mavlink_msg_adsb_msg_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_adsb_msg_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.traffic_alert_status , packet1.address_type , packet1.address , packet1.latitude , packet1.longitude , packet1.altitude , packet1.misc_indicator , packet1.NIC , packet1.NACp , packet1.h_velocity , packet1.v_velocity , packet1.heading );
	mavlink_msg_adsb_msg_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_adsb_msg_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_adsb_msg_send(MAVLINK_COMM_1 , packet1.traffic_alert_status , packet1.address_type , packet1.address , packet1.latitude , packet1.longitude , packet1.altitude , packet1.misc_indicator , packet1.NIC , packet1.NACp , packet1.h_velocity , packet1.v_velocity , packet1.heading );
	mavlink_msg_adsb_msg_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_yc_test_msg(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_message_t msg;
        uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
        uint16_t i;
	mavlink_yc_test_msg_t packet_in = {
		5,
	};
	mavlink_yc_test_msg_t packet1, packet2;
        memset(&packet1, 0, sizeof(packet1));
        	packet1.data = packet_in.data;
        
        

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_yc_test_msg_encode(system_id, component_id, &msg, &packet1);
	mavlink_msg_yc_test_msg_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_yc_test_msg_pack(system_id, component_id, &msg , packet1.data );
	mavlink_msg_yc_test_msg_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_yc_test_msg_pack_chan(system_id, component_id, MAVLINK_COMM_0, &msg , packet1.data );
	mavlink_msg_yc_test_msg_decode(&msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);

        memset(&packet2, 0, sizeof(packet2));
        mavlink_msg_to_send_buffer(buffer, &msg);
        for (i=0; i<mavlink_msg_get_send_buffer_length(&msg); i++) {
        	comm_send_ch(MAVLINK_COMM_0, buffer[i]);
        }
	mavlink_msg_yc_test_msg_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
        
        memset(&packet2, 0, sizeof(packet2));
	mavlink_msg_yc_test_msg_send(MAVLINK_COMM_1 , packet1.data );
	mavlink_msg_yc_test_msg_decode(last_msg, &packet2);
        MAVLINK_ASSERT(memcmp(&packet1, &packet2, sizeof(packet1)) == 0);
}

static void mavlink_test_yc_msg(uint8_t system_id, uint8_t component_id, mavlink_message_t *last_msg)
{
	mavlink_test_adsb_msg(system_id, component_id, last_msg);
	mavlink_test_yc_test_msg(system_id, component_id, last_msg);
}

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // YC_MSG_TESTSUITE_H
